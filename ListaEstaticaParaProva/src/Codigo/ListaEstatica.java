package Codigo;

public class ListaEstatica<T> implements TDAListaEstatica<T> {
	private T[] elementos;
	private int quantidade;
	
	@SuppressWarnings("unchecked")
	public ListaEstatica() {
		this.elementos = (T[]) new Object[10];
		this.quantidade = 0;
	}
	
	@SuppressWarnings("unchecked")
	public ListaEstatica(int capacidade) {
		this.elementos = (T[]) new Object[capacidade];
		this.quantidade = 0;
	}
	
	@Override
	public void incluir(T elemento) {
		if(this.estaCheia()) {this.aumentarCapacidade();}
		this.elementos[this.quantidade++] = elemento;
	}

	@Override
	public void incluirInicio(T elemento) {
		if(this.estaVazia()) {
			this.elementos[this.quantidade++] = elemento;
		}else {
			this.quantidade++;
			int i;
			for (i = this.quantidade; i > 0; i--) {
				this.elementos[i] = this.elementos[i - 1];
			}
			this.elementos[i] = elemento;
		}
	}

	@Override
	public void incluir(T elemento, int posicao) {
		if(this.posicaoInvalida(posicao)) {
			System.out.println("A posicao � invalida.");
		}
		else if(this.estaVazia()) {
			this.incluirInicio(elemento);
		}else {
			if(this.estaCheia()) {
				this.aumentarCapacidade();
			}
			this.quantidade++;
			int i;
			for (i = this.quantidade; i > posicao; i--) {
				this.elementos[i] = this.elementos[i - 1];
			}
			this.elementos[i] = elemento;
		}
		
	}

	@Override
	public T obterDaPosicao(int posicao) {
		if(this.estaVazia() || this.posicaoInvalida(posicao)) {
			return null;
		}
		return this.elementos[posicao];
	}

	@Override
	public int obter(T item) {
		for (int i = 0; i < this.tamanho(); i++) {
			if(this.elementos[i].equals(item)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public void remover(int posicao) {
		if(this.estaVazia() || this.posicaoInvalida(posicao)) {
			System.out.println("A lista est� vazia ou a posicao nao e valido.");
		}else {
			int i;
			for (i = posicao; i < elementos.length; i++) {
				this.elementos[i] = this.elementos[i + 1];
			}
			this.elementos[i] = null;
			this.quantidade--;
		}
	}

	@Override
	public void remover(T elemento) {
		if(this.estaVazia()) {
			System.out.println("A lista esta vazia.");
		}
		for (int i = 0; i < elementos.length; i++) {
			if(this.elementos[i].equals(elemento)) {
				for (int j = i; j < elementos.length; j++) {
					this.elementos[j] = this.elementos[j + 1];
				}
			}
		}
		this.elementos[this.quantidade] = null;
		this.quantidade--;
	}

	@Override
	public void limpar() {
		if(this.estaVazia()) {
			System.out.println("Nao tem elementos para limpar.");
		}else {
			for (int i = 0; i < elementos.length; i++) {
				this.elementos[i] = null;
			}
		}
		this.quantidade = 0;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void aumentarCapacidade() {
		T[] aux = (T[]) new Object[this.elementos.length * 2];
		for (int i = 0; i < this.elementos.length; i++) {
			aux[i] = this.elementos[i];
		}
		this.elementos = aux;
	}

	@Override
	public int tamanho() {
		return this.quantidade;
	}

	@Override
	public boolean contem(T item) {
		if(this.estaVazia()) {
			return false;
		}
		for (int i = 0; i < this.tamanho(); i++) {
			if(this.elementos[i].equals(item)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean verificarInicializacao() {
		return false;
	}

	@Override
	public boolean estaVazia() {
		return this.quantidade == 0;
	}
	
	protected boolean posicaoInvalida(int posicao) {
		return posicao < 0 || posicao > this.quantidade;
	}

	@Override
	public boolean estaCheia() {
		return this.quantidade == this.elementos.length;
	}
	
	@Override
	public String toString() {
		String print = "Fila = [";
		int i;
		for (i = 0; i < this.tamanho() - 1; i++) {
			print = print + this.elementos[i] + ", "; 
		}
		print = print + this.elementos[i] + "] - tamanho = " + this.quantidade;
		return print;
	}

}
