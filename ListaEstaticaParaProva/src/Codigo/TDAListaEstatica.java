package Codigo;

public interface TDAListaEstatica<T> {
	
    public void incluir(T elemento);
    public void incluirInicio(T elemento);
    public void incluir(T elemento, int posicao);
    public T obterDaPosicao(int posicao);
    public int obter(T item);
    public void remover(int posicao);
    public void remover(T elemento);
    public void limpar();
    public void aumentarCapacidade();
    public int tamanho();
    public boolean contem(T item);
    public boolean verificarInicializacao();
    public boolean estaVazia();
    public boolean estaCheia();
}
